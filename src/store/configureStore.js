import {createStore, combineReducers} from 'redux';
import StudentReducer from '../reducer/StudentReducer';
import EnterpriseReducer from '../reducer/EnterpriseReducer';
import authReducer from '../reducer/authReducer';
import contractReducer from '../reducer/contractReducer';



export default ()=>{
    const store = createStore(
        combineReducers({
            students : StudentReducer,
            enterprises : EnterpriseReducer,
            auth : authReducer,
            contracts: contractReducer

        }),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    return store;
};