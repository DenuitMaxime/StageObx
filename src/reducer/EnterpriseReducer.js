const enterpriseDefaultState =[];


const EnterpriseReducer =(state = enterpriseDefaultState,action)=>{
    let my_enter;
    let my_state;
    switch(action.type){
        case 'ADD_ENTERPRISE':
            return [...state, action.enterprise];
        case 'EDIT_ENTERPRISE':
            my_enter = state.find((enter)=>enter.id === action.id);
            Object.assign(my_enter,my_enter,action.update);            
                return state;
        case 'ADD_ALL_COMP':
            my_state = state;
            Array.prototype.push.apply(my_state,action.update);
                return state;
        case 'RESET':
            return enterpriseDefaultState;

        default:
            return state;
    }
};


export default EnterpriseReducer;