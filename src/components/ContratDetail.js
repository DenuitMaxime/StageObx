import React from 'react';


export default (props)=>(
    <div className="section">
        <h4>Contrat de Stage chez {props.contract.ContractName}</h4>
        <h4>Superviseur</h4>
        <p className="flow-text">Nom : {props.contract.ContractSupervisorName}</p>
        <p className="flow-text">Prénom : {props.contract.ContractSupervisorFirstName }</p>
        <p className="flow-text">Mail: {props.contract.ContractSupervisorMail }</p>
        <p className="flow-text">Téléphone: {props.contract.ContractSupervisorPhone }</p>
        <h4>Addresse</h4>
        <p className="flow-text">Rue: {props.contract.ContractAddressStreet }  {props.contract.ContractAddressNumber  } {props.contract.ContractAddressBox  } </p>
        <p className="flow-text">Code Postal: {props.contract.ContractAddressPostalCode }</p>
        <p className="flow-text">Ville: {props.contract.ContractAddressCity }</p>
        <h4>Stage</h4>
        <p className="flow-text">Sujet: {props.contract.ContractSubject }</p>
        <p className="flow-text">Modalité Pratique: {props.contract.ContractModality }</p>

        <h4>Personne de contact</h4>
        <p className="flow-text">Titre: {props.contract.ContractContactTitle }</p>
        <p className="flow-text">Nom: {props.contract.ContractContactName }</p>
        <p className="flow-text">Téléphone: {props.contract.ContractPhone }</p>
        <h4>Information pratique</h4>
        <p className="flow-text">Instruction Supplémentaire:{props.contract.ContractOptionnalInstruction}</p>
        <p className="flow-text">heure d'arrivé:{props.contract.ContractArrivalTime}</p>
        <p className="flow-text">Remarques:{props.contract.ContractNotes}</p>
    </div>
)