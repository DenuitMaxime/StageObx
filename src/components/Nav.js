import React from 'react';
import {NavLink}   from 'react-router-dom';
import {connect} from 'react-redux';
import {resetStore} from '../action/studentAction'


const Nav = (props)=>(
        <nav className="nav-wrapper white">
            <div className="container">
            
                <NavLink to="/" className="brand-logo blue-grey-text">StageObx</NavLink>
                <ul className="right hide-on-med-down">
                    {
                        sessionStorage.getItem('token') &&
                        (<li><NavLink  to="/"><button className="nav white-text btn blue-grey" onClick={()=>{
                            sessionStorage.clear();
                            //props.dispatch(resetStore());

                            
                        }}>Deconnexion</button></NavLink></li>)
                        
                    }
                </ul>
            </div>
        </nav>

);


export default connect()(Nav);