import React from 'react';
import ContractFrom from './ContractForm';
import {connect} from 'react-redux';
import {changeContract} from '../action/studentAction';
import axios from 'axios';



const AddContract = (props)=>(
    <div className="container">
        <h3>Ajouter votre contrat</h3>
        <ContractFrom onSubmit={(contract)=>{
            
            axios({
                method:'post',
                url:'https://stageobx.azurewebsites.net/Contract',
                data:{
                    "ContractDTO" : contract

                },
                headers:{
                    'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                }
                 
            }).then(function(response){
                console.log(response);
            });
            props.dispatch(changeContract(contract,props.match.params.id))
            props.history.go(-1);
        }} />
    </div>

);

export default connect()(AddContract);