import React from 'react';
import {connect} from 'react-redux';
import EntrepriseForm from './EntrepriseForm';
import {editEnterprise} from '../action/studentAction';
import axios from 'axios'


const EditEntrepriseDetails = (props)=>(
    <div className="container row">
        <h1>Editer une Entreprise</h1>
        <p className="bold">* Champs Obligatoires</p>
        <EntrepriseForm enterprise={props.enterprise} onSubmit={
            (enterprise)=>{
                enterprise.student = sessionStorage.getItem('id');
                console.log('editdetails');
                props.dispatch(editEnterprise(enterprise,enterprise.Id));
                axios({
                    method:'put',
                    url : 'https://stageobx.azurewebsites.net/Company',
                    data:{
                        CompanyDTO:{
                            Id: props.match.params.id,
                            Name: enterprise.Name,
                            City: enterprise.City,
                            StreetName: enterprise.StreetName,
                            PostalCode: enterprise.PostalCode,
                            Telephone : enterprise.Telephone
                        }
                    },
                    headers:{
                        'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                    }
                }).then((response)=>{
                    console.log(response);
                    props.history.push(`/student/${props.match.params.id}`);
                })
        }} />
    </div>
);

const mapStateToProps = (state,props)=>{
    console.log(state.enterprises.find((entreprise)=>
    entreprise.Id === props.match.params.id))
    return{
        enterprise: state.enterprises.find((entreprise)=>
            entreprise.Id.toString() === props.match.params.id)
        
    };
};

export default connect(mapStateToProps)(EditEntrepriseDetails);