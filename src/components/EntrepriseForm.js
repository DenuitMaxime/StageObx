import React from 'react';
import uuid from 'uuid';




export default class EntrepriseForm extends React.Component{
    constructor(props){
        super(props)
        this.state={
            id:props.enterprise ? props.enterprise.Id : uuid(),
            street: props.enterprise ? props.enterprise.StreetName :'',
            postalCode: props.enterprise ? props.enterprise.PostalCode :'',
            city: props.enterprise ? props.enterprise.City :'',
            name: props.enterprise ? props.enterprise.Name :'',
            phone: props.enterprise ? props.enterprise.Telephone :'',
            error:''
        }
    }
    onCommonChange=(e)=>{
        const target = e.target;
        let value= target.value;
        const name = target.name;

        this.setState({
            [name]:value
        });
        console.log('onCommonChange');
    }

    onSubmit=(e)=>{
        e.preventDefault();
        if(! this.state.name || !this.state.street  
            || !this.state.postalCode || !this.state.city || !this.state.phone){
                this.setState(()=>({error:"Veuillez remplir tous les champs"}));
                window.scrollTo(0,0);

            }else{
                this.setState(()=>({error:''}));
                this.props.onSubmit({
                    id: this.state.id,
                    Name:this.state.name,
                    StreetName:this.state.street,
                    PostalCode:this.state.postalCode,
                    City:this.state.city,
                    Telephone:this.state.phone,
                    Student:''
                });
            }
    }

    render(){
        return(
            <div className="row">
                {this.state.error && <p className="flow-tex bold red-text">{this.state.error}</p>}
                <form className="col s12"  onSubmit={this.onSubmit}>
                    <div className="row section">

                        <input 
                        className="validate"
                        placeholder="Nom de l'entreprise*"
                        value={this.state.name} 
                        name='name' 
                        onChange={this.onCommonChange} 
                        autoFocus type="text" />
                        
                        <input 
                        className="validate"
                        placeholder="Téléphone*"
                        value={this.state.phone} 
                        name='phone' 
                        onChange={this.onCommonChange} 
                        type="text" />
                        
                        <h6>Adresse</h6>
                        <input 
                        className="validate"
                        placeholder="Rue*"
                        value={this.state.street} 
                        name='street'
                        onChange={this.onCommonChange} 
                        type="text" />
                        
                        <input
                        className="validate" 
                        placeholder="Code Postal"
                        value={this.state.postalCode} 
                        name='postalCode' 
                        onChange={this.onCommonChange} 
                        type="text" />
                        
                        <input 
                        className="validate"
                        placeholder="Ville*"
                        value={this.state.city} 
                        name='city' 
                        onChange={this.onCommonChange} 
                        type="text" />
                             
                        
                        
                    </div>
                    <button className="btn blue-grey white-text">Ajouter</button>
                </form>
            </div>
        )
    }

}