import React from 'react';
import Connect from './Connect';
import {Link, Redirect, withRouter} from 'react-router-dom';
import axios from 'axios';



 class Auth extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            auth : ''
        }
    }
    
    onAuthChange=()=>{
        if(sessionStorage.getItem('auth') === 'student'){
            this.props.history.push('/');
        }else if (sessionStorage.getItem('auth') ==='teacher'){
            this.props.history.push('/');
        }
        console.log(this.props.history);
    }
    
    render(){
        return(
            <div className=" container row">
                <div className=" col s12 center-align auth">  
                    <Connect  onSubmit={(conn)=>{
                        let token = sessionStorage.getItem('token');
                        if(!token){
                            console.log(conn)
                            let data = "grant_type=password&username=" + conn.mail + "&password=" + conn.pwd;
                            axios({
                                method: 'post',
                                url : 'https://stageobx.azurewebsites.net/token',
                                data : data,
                                headers:{
                                    'Content-Type' : 'application/x-www-form-urlencoded'
                                }
                            }).then((response)=>{
                                console.log('login');
                                token = response.data.access_token;
                                sessionStorage.setItem('token',token);
                                axios({
                                    method: 'get',
                                    url: 'https://stageobx.azurewebsites.net/Admin',
                                    headers:{
                                        'Authorization' : 'bearer '+ token
                                    }
                                }).then((response)=>{
                                    console.log('admin' + response);
                                    if(response.data === true){
                                        sessionStorage.setItem('auth','teacher');
                                        this.props.history.push('/');
                                    }else{
                                        sessionStorage.setItem('auth','student');
                                        axios({
                                            method: 'get',
                                            url: 'https://stageobx.azurewebsites.net/Student',
                                            headers:{
                                                'Authorization' : 'bearer '+ token
                                            }
                                        }).then((response)=>{
                                            console.log(response.data[0].Id.toString());
                                            sessionStorage.setItem('id', ''+response.data[0].Id);
                                            console.log(response);
                                            this.props.history.push('/');
                                        })
                                    }
                                });
                                
                                
                            });
                        }else{
                            //this.onAuthChange();
                            this.props.history.push('/');
                            
                        }

                    
                        
                    }} />
                    <div className="section">
                        <p className="flow-text s6" >Pas encore de compte ?</p><Link className="s6 blue-grey-text" to='/inscr'>Inscrivez-vous ici</Link>
                    </div>
                </div>
            </div>
        )
    }
};

export default withRouter(Auth);