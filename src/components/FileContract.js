import React from 'react';
import ReactFileReader from 'react-file-reader';
import axios from 'axios';



export default class FileContract extends React.Component{
    constructor(props){
        super(props);
    }
    handleFile=(file)=>{
        console.log(file);
        axios({
            method:'put',
            url:'https://stageobx.azurewebsites.net/Student',
            data:{
                "StudentDTO" : {
                    "Document": file.base64
                }

            },
            headers:{
                'Authorization' : 'bearer '+ sessionStorage.getItem('token')
            }

        }).then(function(response){
            console.log(response);
            

        });
    }
    
    render(){
        return(
            <div className="section">
                <h4>Ajouter un Document</h4> 
                <ReactFileReader handleFiles={this.handleFile} fileTypes={".pdf"} base64={true}>
                    <button className="btn blue-grey white-text">File</button>
                </ReactFileReader>
            </div>
        )
    }
}