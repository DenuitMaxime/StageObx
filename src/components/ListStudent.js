import React from 'react';
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import axios from 'axios';
import {addAll} from '../action/studentAction';



class ListStudent extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            students: [],
            filteredStudents: [],
            filter:'',
            ok:''
        }
        
    }
    componentWillMount=()=>{
        if(sessionStorage.getItem('auth')=== 'student'){
            this.props.history.push('/student/1');
        }else if(!sessionStorage.getItem('auth')){
            this.props.push('/');
        }

        if(this.props.students.length === 0){
            let students;
            axios({
                method: 'get',
                url : 'https://stageobx.azurewebsites.net/Student/0',
                headers:{
                    'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                }
            }).then((response)=>{
                console.log(response);
                students = response.data;
                this.props.dispatch(addAll(students));
                this.setState(()=>({students :this.props.students}))
                this.setState(()=>({filteredStudents : this.props.students}));
                this.setState(()=>({ok :'done'}));
                console.log(this.state.students);
            })
        }else{
            this.setState(()=>({students :this.props.students}))
            this.setState(()=>({filteredStudents : this.props.students}));
            this.setState(()=>({ok :'done'}));
        }
        if (sessionStorage.getItem('id')){
            sessionStorage.removeItem('id');
        };
        
    }        
    onfilterChange=(e)=>{
        let filter = e.target.value;
        this.setState(()=>({filter}));
        let updatedStudents = this.state.students;
        updatedStudents = updatedStudents.filter((student)=>{
            return student.Name.toLowerCase().search(filter.toLowerCase()) !== -1 || student.FirstName.toLowerCase().search(filter.toLowerCase()) !== -1
            || student.Email.toLowerCase().search(filter.toLowerCase()) !== -1
        });
        this.setState(()=>({filteredStudents: updatedStudents}));
    
    }
    render(){
        return (
            <div className="container row">
                <h4 className="align-center blue-grey-text col s6">Liste des étudiants</h4>
                <input 
                className="col s3"
                type='text' 
                value={this.state.filter} 
                placeholder='search' 
                onChange={this.onfilterChange} />
                
                <div className="col s12">
                    {
                        this.state.ok === 'done' && this.props.students.length === 0 ?
                        (<p>Aucun étudiants</p>)
                        :
                            <table className=" section highlight">
                                <thead>
                                    <tr>
                                        <th>Prénom</th>
                                        <th>Nom</th>
                                        <th>Mail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.filteredStudents.map((student)=>{
                                            return <tr key={student.Id} onClick={()=> this.props.history.push(`/student/${student.Id}`)}>
                                                        <td>{student.FirstName}</td>
                                                        <td>{student.Name}</td>
                                                        <td>{student.Email}</td>
                                                    </tr>})
                                    }
                                </tbody>
                            </table>
                            
                        
                    }
                </div>

    </div>
        )
    }
   
};

const mapStateToProps=(state)=>{
    return{
        students: state.students
    }
};

export default connect(mapStateToProps)(ListStudent);