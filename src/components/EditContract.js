import React from 'react';
import {connect} from 'react-redux';
import ContractForm from './ContractForm';
import {changeContract ,deleteContract} from '../action/studentAction';
import axios from "axios"

const EditContract = (props)=>(
    <div className="container">
        <h3>Editer votre contrat</h3>
        <ContractForm contract={props.contract} onSubmit={(contract)=>{
            props.dispatch(changeContract(contract,props.match.params.id))
            axios({
                method:'put',
                url:'https://stageobx.azurewebsites.net/Contract',
                data:{
                    "ContractDTO" : contract

                },
                headers:{
                    'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                }
                 
            }).then(function(response){
                console.log(response);
                props.history.go(-1);
                
            });
            
            }} /> 
    </div>

);

const mapStateToProps=(state,props)=>{
    console.log(state.contracts[0])

    return{
        contract: state.contracts[0]
          }
};

export default connect(mapStateToProps)(EditContract);