import React from 'react';
import InscriptionForm from './InscriptionForm';
import axios from 'axios';



export default class Inscription extends React.Component{
    constructor(props){
        super(props);
    };
    render(){
        return(
            <div className="container center-align">
                <h1>Inscription</h1>
                <p className="bold">* Champs Obligatoires</p>
                <InscriptionForm onSubmit={(inscr)=>{
                    axios({
                        method: 'post',
                        url : 'https://stageobx.azurewebsites.net/api/Account/Register',
                        data: {
                            "Email":inscr.mail,
                            "Password":inscr.pwd1,
                            "ConfirmPassword":inscr.pwd2
                        }
                    }).then((response)=>{
                       let data = "grant_type=password&username=" + inscr.mail + "&password=" + inscr.pwd1;
                       let token;
                       axios({
                            method: 'post',
                            url : 'https://stageobx.azurewebsites.net/token',
                            data : data,
                            headers:{
                                'Content-Type' : 'application/x-www-form-urlencoded'
                            }
                       }).then((response)=>{
                            token = response.data.access_token;
                            sessionStorage.setItem('token',token);
                            axios({
                                method: 'post',
                                url : 'https://stageobx.azurewebsites.net/Student',
                                data:{
                                   "StudentDTO":{
                                        "Name": inscr.name,
                                        "FirstName": inscr.firstName,
                                        "Departement" : "2BIN",
                                        "Telephone":inscr.phone,
                                        "Email":inscr.mail
                                    }
                                },
                                headers:{
                                    'Authorization' : 'bearer '+ token
                                }
                            }).then((response)=>{
                                console.log(response);
                                sessionStorage.setItem('auth','student');
                                axios({
                                    method: 'get',
                                    url: 'https://stageobx.azurewebsites.net/Student',
                                    headers:{
                                        'Authorization' : 'bearer '+ token
                                    }
                                }).then((response)=>{
                                    console.log(response.data[0].Id.toString());
                                    sessionStorage.setItem('id', ''+response.data[0].Id);
                                    console.log(response);
                                    this.props.history.push('/');
                                })
                            })
                       })
                    }).catch((error)=>{
                        console.log(error);
                    })
                    }} />
            </div>
        );
    };
};