import React from 'react';



class Connect extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            mail:'',
            pwd:'',
            error:''
        }

    }
    onCommonChange=(e)=>{
        const target = e.target;
        const name = target.name;
        const value = target.value;
        this.setState(()=>({[name]:value}));
    }
    onSubmit=(e)=>{
        e.preventDefault();
        if(!this.state.mail || !this.state.pwd){
            this.setState(()=>({error:'Veuillez remplir tous les champs'}));
            window.scrollTo(0,0);
        }else{
            this.setState(()=>({error:''}));
            this.props.onSubmit({
                mail: this.state.mail,
                pwd : this.state.pwd
            })
            console.log('connect');
        }
    }
    render(){
        return(
            <div>
                {this.state.error && <p>{this.state.error}</p>}
                <form onSubmit={this.onSubmit}>
                    <input 
                        type="text" 
                        placeholder="Mail" 
                        value={this.state.mail} 
                        name='mail' 
                        onChange={this.onCommonChange}/>
                    <input 
                        type="password" 
                        placeholder="Mot de passe" 
                        pwd={this.state.pwd} 
                        name='pwd' 
                        onChange={this.onCommonChange}/>
                    <button className="btn blue-grey white-text">Connection</button>
                </form>
            </div>
        );
    };
};


export default Connect;