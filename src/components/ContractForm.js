import React from 'react';


export default class ContractForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: props.contract ? props.contract.ContractName  : '',
            supervisor:{
                name: props.contract ? props.contract.ContractSupervisorName   : '',
                firstName: props.contract ? props.contract.ContractSupervisorFirstName  : '',
                mail: props.contract ? props.contract.ContractSupervisorMail  :'',
                phone: props.contract ? props.contract.ContractSupervisorPhone  :''
            },
            address:{
                street: props.contract ? props.contract.ContractAddressStreet :'',
                number: props.contract ? props.contract.ContractAddressNumber  :'',
                box: props.contract ? props.contract.ContractAddressBox  : '',
                postalCode: props.contract ? props.contract.ContractAddressPostalCode  :'',
                city: props.contract ? props.contract.ContractAddressCity  : ''
            },
            subject: props.contract ? props.contract.ContractSubject  :'',
            modality: props.contract ? props.contract.ContractModality  :'',
            contact:{
                title: props.contract ? props.contract.ContractContactTitle  :'',
                name: props.contract ? props.contract.ContractContactName :'',
                phone: props.contract ? props.contract.ContractPhone  :''
            },
            optionnalInstruction: props.contract ? props.contract.ContractOptionnalInstruction  : '',
            arrivalTime: props.contract ? props.contract.ContractArrivalTime  :'',
            notes: props.contract ? props.contract.ContractNotes  :'',
            error:''
        };
    };

    onCompagnyNameChange = (e)=>{
        const name = e.target.value;
        this.setState(()=>({name}));
    }
    onSupervisorNameChange =(e)=>{
        const name = e.target.value;
        const supervisor = this.state.supervisor;
        supervisor.name = name;
        this.setState(()=>({supervisor}))
    }
    onSupervisorFirstNameChange =(e)=>{
        const firstName = e.target.value;
        const supervisor = this.state.supervisor;
        supervisor.firstName = firstName;
        this.setState(()=>({supervisor}))
    }
    onSupervisorMailChange =(e)=>{
        const mail = e.target.value;
        const supervisor = this.state.supervisor;
        supervisor.mail = mail;
        this.setState(()=>({supervisor}))
    }
    onSupervisorPhoneChange =(e)=>{
        const phone = e.target.value;
        const supervisor = this.state.supervisor;
        supervisor.phone = phone;
        this.setState(()=>({supervisor}))
    }
    onStreetChange=(e)=>{
        const street = e.target.value;
        const address = this.state.address;
        address.street = street;
        this.setState(()=>({address}));
    }
    onStreetNumberChange=(e)=>{
        const number = e.target.value;
        const address = this.state.address;
        address.number = number;
        this.setState(()=>({address}));
    }
    onBoxChange=(e)=>{
        const box = e.target.value;
        const address = this.state.address;
        address.box = box;
        this.setState(()=>({address}));
    }
    onPostalCodeChange=(e)=>{
        const postalCode = e.target.value;
        const address = this.state.address;
        address.postalCode = postalCode;
        this.setState(()=>({address}));
    }
    onCityChange=(e)=>{
        const city = e.target.value;
        const address = this.state.address;
        address.city = city;
        this.setState(()=>({address}));
    }
    onSubjectChange=(e)=>{
        const subject = e.target.value;
        this.setState(()=>({subject}));
    }
    onModalityChange=(e)=>{
        const modality = e.target.value;
        this.setState(()=>({modality}));
    }
    onContactTitleChange=(e)=>{
        const title = e.target.value;
        const contact = this.state.contact;
        contact.title = title;
        this.setState(()=>({contact}));

    }
    onContactNameChange=(e)=>{
        const name = e.target.value;
        const contact = this.state.contact;
        contact.name = name;
        this.setState(()=>({contact}));

    }
    onContactPhoneChange=(e)=>{
        const phone = e.target.value;
        const contact = this.state.contact;
        contact.phone = phone;
        this.setState(()=>({contact}));

    }
    onOptionnalInstructionChange=(e)=>{
        const optionnalInstruction = e.target.value;
        this.setState(()=>({optionnalInstruction}));
    }
    onArrivalTimeChange=(e)=>{
        const arrivalTime = e.target.value;
        this.setState(()=>({arrivalTime}));
    }
    onNoteChange=(e)=>{
        const notes = e.target.value;
        this.setState(()=>({notes}));
    }
    onSubmit = (e)=>{
        e.preventDefault();
         if(!this.state.name /*|| !this.state.contact.name || !this.state.contact.phone || !this.state.contact.title || !this.state.modality 
        // || !this.state.subject || !this.state.supervisor.name || !this.state.supervisor.firstName || !this.state.supervisor.mail 
        // || !this.state.supervisor.phone || !this.state.address.street || !this.state.address.box || !this.state.address.number
         // || !this.state.address.postalCode || !this.state.address.city || !this.state.arrivalTime || !this.state.optionnalInstruction*/){
            this.setState(()=>({error: 'veuillez complèter tous les champs obligatoire'}));
            window.scrollTo(0,0);
        }else{
            this.setState(()=>{error:''});
            this.props.onSubmit({
                ContractName  : this.state.name,
                ContractSupervisorName  : this.state.supervisor.name,
                ContractSupervisorFirstName   : this.state.supervisor.firstName,
                ContractSupervisorMail   : this.state.supervisor.mail,
                ContractSupervisorPhone   : this.state.supervisor.phone,
                ContractAddressStreet  : this.state.address.street,
                ContractAddressNumber   : this.state.address.number,
                ContractAddressBox   : this.state.address.box,
                ContractAddressPostalCode   : this.state.address.postalCode,
                ContractAddressCity   : this.state.address.city,
                ContractSubject  : this.state.subject,
                ContractModality : this.state.modality,
                ContractContactTitle : this.state.contact.title,
                ContractContactName  : this.state.contact.name,
                ContractPhone  : this.state.contact.phone,
                ContractOptionnalInstruction : this.state.optionnalInstruction,
                ContractArrivalTime : this.state.arrivalTime,
                ContractNotes : this.state.notes
            })
            console.log('AddFormCompleted');
        }

    }
    render(){
        return(
            <div className="row">
                {this.state.error && <p className="flow-tex bold red-text">{this.state.error}</p>}
                <form className="col s12" onSubmit={this.onSubmit}>
                   <div className="row section">
                    <input
                        placeholder="Nom de la société*"
                        type='text' 
                        autoFocus 
                        value={this.state.name}
                        onChange={this.onCompagnyNameChange}
                    />
                    <p>Personne responsable du stage dans la société</p>
                        <div>
                            
                            <input 
                                placeholder="Nom*"
                                type='text'
                                value={this.state.supervisor.name}
                                onChange={this.onSupervisorNameChange}
                            />
                
                            <input 
                                placeholder="Prénom*"
                                type='text'
                                value={this.state.supervisor.firstName}
                                onChange={this.onSupervisorFirstNameChange}
                            />

                            <input 
                                placeholder="Mail*"
                                type='email'
                                value={this.state.supervisor.mail}
                                onChange={this.onSupervisorMailChange}
                            />
    
                            <input 
                                placeholder="Téléphone*"
                                type='text'
                                value={this.state.supervisor.phone}
                                onChange={this.onSupervisorPhoneChange}
                            />
                        </div>
                    <p>Adresse du stage</p>
                        <div>
                            
                            <input 
                                placeholder="Rue*"
                                type='text'
                                value={this.state.address.street}
                                onChange={this.onStreetChange}
                            />
                            
                            <input 
                                placeholder="Numéro*"
                                type='text'
                                value={this.state.address.number}
                                onChange={this.onStreetNumberChange}
                            />
                            
                            <input 
                                placeholder='Boite'
                                type='text'
                                value={this.state.address.box}
                                onChange={this.onBoxChange}
                            />
                            
                            <input 
                                placeholder="Code Postal*"
                                type='text'
                                value={this.state.address.postalCode}
                                onChange={this.onPostalCodeChange}
                            />
                            
                            <input 
                                placeholder="Ville*"
                                type='text'
                                value={this.state.address.city}
                                onChange={this.onCityChange}
                            />
                        </div>
                        <p>Stage</p>  
                    <input
                        placeholder="Sujet du Stage*"
                        type='text'
                        value={this.state.subject}
                        onChange={this.onSubjectChange}
                    />
                    
                    <input
                        placeholder="Modalités pratiques*"
                        type='text'
                        value={this.state.modality}
                        onChange={this.onModalityChange}
                    />
                    <p>Personne à contacter par l'étudiant le premier jour</p>    
                        <div>
                            
                            <input
                                placeholder="Titre*"
                                type='text'
                                value={this.state.contact.title}
                                onChange={this.onContactTitleChange}
                            />  
                            
                            <input
                                placeholder="Nom*"
                                type='text'
                                value={this.state.contact.name}
                                onChange={this.onContactNameChange}
                            />
                            
                            <input
                                placeholder="Téléphone*"
                                type='text'
                                value={this.state.contact.phone}
                                onChange={this.onContactPhoneChange}
                            />
                        </div>
                        <p>Consignes</p>  
                    <input
                        placeholder="Consignes éventuelles"
                        type='text'
                        value={this.state.optionnalInstruction}
                        onChange={this.onOptionnalInstructionChange}
                    />
                    
                    <input
                        placeholder="Heure d'arrivée le premier jour*"
                        type='text'
                        value={this.state.arrivalTime}
                        onChange={this.onArrivalTimeChange}
                    />
                    
                    <input
                        placeholder="Remarques éventuelles à communiquer à l'étudiant avant son entrée dans la société"
                        type='text'
                        value={this.state.notes}
                        onChange={this.onNoteChange}
                    />
                    <br/>
                    
                    </div>
                    <button className="btn blue-grey white-text">Ajouter</button>
                </form>
            </div>    
        );
        
    };
};

