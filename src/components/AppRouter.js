import React from 'react';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import ListEntreprise from './ListEntreprise';
import ListStudent from './ListStudent';
import Nav from './Nav';
import EditEntrepriseDetails from './EditEntrepriseDetails';
import AddEnterprise from './AddEntreprise';
import AddContract from './AddContract';
import EditContract from './EditContract';
import Auth from './Auth';
import Inscription from './Inscription';
import {connect} from 'react-redux';



class AppRouter extends React.Component{
    constructor(props){
        super(props);

    };
    
    render(){
        return (
            <BrowserRouter>
            <div>
                <Nav />
                <Switch>
                <Route path="/student/:id" component={ListEntreprise}  />
                    <Route path='/' exact render={()=>(
                        !!sessionStorage.getItem('auth') ?
                        (
                             sessionStorage.getItem('auth') === 'student' ?
                            (
                                <Redirect to={`/student/${sessionStorage.getItem('id')}`}/>
                            ):
                            (
                                <Redirect to='/listStudent' />
                            )
                        ):
                        (
                            <Auth/>
                        )
                    )} />
                    <Route path='/listStudent' exact component={ListStudent} />
                    
                    <Route path="/enterprise/:id" component={EditEntrepriseDetails} />
                    <Route path="/add/:id" component={AddEnterprise} />
                    <Route path="/addContract/:id" component={AddContract} />
                    <Route path='/editContract/:id' component={EditContract}/>
                    <Route path='/inscr' component={Inscription} />
                    <Route component={Auth} />
    
                </Switch>
            </div>  
        </BrowserRouter>
        )
    }
};



export default connect()(AppRouter);