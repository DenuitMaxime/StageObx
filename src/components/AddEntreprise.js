import React from 'react';
import EntrepriseForm from './EntrepriseForm';
import {connect} from 'react-redux';
import {addEnterprise} from '../action/studentAction'
import axios from 'axios';


const AddEnterprise = (props)=>(
    <div className="container row">
        <h1>Ajouter une Entreprise</h1>
        <p className="bold">* Champs Obligatoires</p>
        <EntrepriseForm 
            onSubmit={(enterprise) =>{
                enterprise.student = props.match.params.id;
                props.dispatch(addEnterprise(enterprise));
                let token = sessionStorage.getItem('token');
                console.log(enterprise);
                axios({
                    method:'post',
                    url : 'https://stageobx.azurewebsites.net/Company',
                    data:{
                        CompanyDTO:{
                            Name: enterprise.Name,
                            City: enterprise.City,
                            StreetName: enterprise.StreetName,
                            PostalCode: enterprise.PostalCode,
                            Telephone : enterprise.Telephone
                        }
                    },
                    headers:{
                        'Authorization' : 'bearer '+ token
                    }
                }).then((response)=>{
                    console.log(response);
                    props.history.push(`/student/${props.match.params.id}`);
                })

                
                
               
            }} 
        />
    </div>
);

export default connect()(AddEnterprise);
