import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import _ from 'lodash';
import FileContract from './FileContract';
import axios from 'axios';
import {addAllComp, addAllContract} from '../action/studentAction';
import ContractDetail from './ContratDetail';



class ListEntreprise  extends React.Component{ 
    constructor(props){
        super(props);
        this.state = {
            company:  [],
            contract: '',
            doc: false,
            docu: '',
            ok:''
        }
    }
    componentWillReceiveProps=(nextProps)=>{
        if(sessionStorage.getItem('auth') === "teacher"){
            if( !this.props.contract && !this.props.enteprises){
                axios({
                    method: 'get',
                    url : 'https://stageobx.azurewebsites.net//CompanyStudent/'+nextProps.match.params.id,
                    headers:{
                        'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                    }
                }).then((response)=>{
                    console.log(response);
                    companies = response.data;
                    this.props.dispatch(addAllComp(companies));
                    this.setState(()=>({company:nextProps.enterprises}));
                    axios({
                        method: 'get',
                        url : 'https://stageobx.azurewebsites.net/Contract/0',
                        headers:{
                            'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                        }
                    }).then((response)=>{
                        console.log(response);
                        contract = response.data;
                        this.props.dispatch(addAllContract(contract));
                        this.setState(()=>({ok :'done'})); 
                        this.setState(()=>({contract: nextProps.contracts[0]}));
                    }) 
                   
                });
            }else{
                this.setState(()=>({ok :'done'})); 
                this.setState(()=>({contract: nextProps.contracts[0]}));
                this.setState(()=>({company:nextProps.enterprises}));
            }
               
        }else{
            if( !nextProps.contract && !nextProps.enteprises){
            axios({
                method: 'get',
                url : 'https://stageobx.azurewebsites.net/Company',
                headers:{
                    'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                }
            }).then((response)=>{
                console.log(response);
                companies = response.data;
                this.props.dispatch(addAllComp(companies));
                this.setState(()=>({company:nextProps.enterprises}));
                axios({
                    method: 'get',
                    url : 'https://stageobx.azurewebsites.net/Contract/',
                    headers:{
                        'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                    }
                }).then((response)=>{
                    console.log(response);
                    contract = response.data;
                    this.props.dispatch(addAllContract(contract));
                    this.setState(()=>({ok :'done'})); 
                    this.setState(()=>({contract: nextProps.contracts}));
                }) 
               
            })
        }else{
            this.setState(()=>({ok :'done'})); 
            this.setState(()=>({contract: nextProps.contracts}));
            this.setState(()=>({company:nextProps.enterprises}));
        }
        }
    
    }
    componentWillMount=()=>{

        //if id != props.id then goback -1
    
        let companies;
        let contract;
        if(this.props.enterprises.length === 0){
            if(sessionStorage.getItem('auth') === "teacher"){
                if( !this.props.contract && !this.props.enteprises){
                    axios({
                        method: 'get',
                        url : 'https://stageobx.azurewebsites.net//CompanyStudent/'+this.props.match.params.id,
                        headers:{
                            'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                        }
                    }).then((response)=>{
                        console.log(response);
                        companies = response.data;
                        this.props.dispatch(addAllComp(companies));
                        this.setState(()=>({company:this.props.enterprises}));
                        axios({
                            method: 'get',
                            url : 'https://stageobx.azurewebsites.net/Contract/0',
                            headers:{
                                'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                            }
                        }).then((response)=>{
                            console.log(response);
                            contract = response.data;
                            this.props.dispatch(addAllContract(contract));
                            this.setState(()=>({ok :'done'})); 
                            this.props.contracts.map((contr)=>{
                                if(contr.StudentId.toString() === sessionStorage.getItem('id')){
                                    console.log("bl",contr)
                                    this.setState(()=>({contract: contr}))
                                }
                            } )
                            
                            axios({
                                method: 'get',
                                url: 'https://stageobx.azurewebsites.net/Student/'+sessionStorage.getItem('id'),
                                headers:{
                                    'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                                }
                            }).then((response)=>{
                                console.log(response);
                                if(response.data[0].Document){
                                    this.setState(()=>({doc:true}))
                                    this.setState(()=>({docu: response.data[0].Document}))
                                }else{
                                    this.setState(()=>({doc:false}))
                                }
                            })
                        }) 
                       
                    });
                }else{
                    this.setState(()=>({ok :'done'})); 
                    this.setState(()=>({contract: this.props.contracts[0]}));
                    this.setState(()=>({company:this.props.enterprises}));
                }
                   
            }else{
                if( !this.props.contract && !this.props.enteprises){
                axios({
                    method: 'get',
                    url : 'https://stageobx.azurewebsites.net/Company',
                    headers:{
                        'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                    }
                }).then((response)=>{
                    console.log(response);
                    companies = response.data;
                    this.props.dispatch(addAllComp(companies));
                    this.setState(()=>({company:this.props.enterprises}));
                    axios({
                        method: 'get',
                        url : 'https://stageobx.azurewebsites.net/Contract/',
                        headers:{
                            'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                        }
                    }).then((response)=>{
                        console.log(response);
                        contract = response.data;
                        this.props.dispatch(addAllContract(contract));
                        this.setState(()=>({ok :'done'})); 
                        this.setState(()=>({contract: this.props.contracts}));
                        axios({
                            method: 'get',
                            url: 'https://stageobx.azurewebsites.net/Student',
                            headers:{
                                'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                            }
                        }).then((response)=>{
                            console.log(response);
                            if(response.data[0].Document){
                                this.setState(()=>({doc:true}))
                            }else{
                                this.setState(()=>({doc:false}))
                            }
                        })
                    }) 
                   
                })
            }else{
                this.setState(()=>({ok :'done'})); 
                this.setState(()=>({contract: this.props.contracts}));
                this.setState(()=>({company:this.props.enterprises}));
            }
            }

            axios({
                method: 'get',
                url: 'https://stageobx.azurewebsites.net/Student',
                headers:{
                    'Authorization' : 'bearer '+ sessionStorage.getItem('token')
                }
            }).then((response)=>{
                console.log(response);
                
            })
        }
        if (!sessionStorage.getItem('id')){
            sessionStorage.setItem('id', this.props.match.params.id);
        };

        if(!sessionStorage.getItem('auth')){
            this.props.history.push('/');
        };
    }

    
    render(){
        let key = 0;
        
        return(
            <div className="container row">
                
                <div className="col s12">
                    <button onClick={(()=> this.props.history.goBack())}>back</button>
                    {
                        sessionStorage.getItem('auth') === 'student' ?
                        (<h3>Vos entreprises</h3>)
                        :
                        (<h3>Les entreprises de l'étudiant</h3>)
                    }

                    {
                        
                         this.state.ok === "done" && this.state.company.length === 0 ?
                        (<p className=" section flow-text bold blue-grey-text">Aucune entreprise</p>)
                        :
                        (<table className=" section highlight">
                            {
                                this.state.company.length > 0 && (
                                    <thead>
                                        <tr>
                                            <th>Nom de l'entreprise</th>
                                            <th>Téléphone</th>
                                            <th>Adresse</th>
                                        </tr>   
                                    </thead>
                                )
                            }
                           
                            <tbody>
                            {   sessionStorage.getItem('auth') === 'teacher'?
                                (this.state.company.map((enterprise)=>{
                                    return <tr key={enterprise.Id}>
                                                <td  key={key++}>{enterprise.Name}</td>
                                                <td  key={key++}>{enterprise.Telephone}</td>
                                                <td key={key++}>{enterprise.StreetName} ,{enterprise.PostalCode} {enterprise.City}</td>
                                            </tr>}))
                                :
                                (this.state.company.map((enterprise)=>{
                                    return <tr key={enterprise.Id} onClick={()=> this.props.history.push(`/enterprise/${enterprise.Id}`)}>
                                                <td  key={key++}>{enterprise.Name}</td>
                                                <td  key={key++}>{enterprise.Telephone}</td>
                                                <td key={key++}>{enterprise.StreetName} ,{enterprise.PostalCode} {enterprise.City}</td>
                                            </tr>}))
                                
                            }
                            </tbody>
                        </table>
                        )  
                    }
                    {
                            sessionStorage.getItem('auth')=== 'student'
                         && (<Link to={`/add/${this.props.match.params.id}`}><button className="btn btn-perso blue-grey white-text">Ajouter une entreprise</button></Link>)
                        
                    }  
                    
                </div>
                <div>
                    {
                            sessionStorage.getItem('auth') === 'teacher' ?
                        (
                            !this.state.contract ?
                            (<h6>Pas de contrat pour cette étudiant</h6>)
                            :
                            (<ContractDetail contract={this.state.contract} />)

                        )
                        :
                        (
                                !this.state.contract  ?
                            (<div className="section">
                                <p>Pas de contrat</p>
                                <Link to={`/addContract/${this.props.match.params.id}`}><button className="btn btn-perso blue-grey white-text">Rajouter un contrat</button></Link>
                            </div>)
                            :
                            (<div className="section">
                                <p>{this.state.contract.ContractName}</p>
                                <Link to={`/editContract/${this.props.match.params.id}`}><button className="btn btn-perso blue-grey white-text">Editer votre contrat</button></Link>
                            </div>)
                        )
                   }
                  
                    
                </div>
                   

                
                {
                    sessionStorage.getItem('auth') === 'student'?
                    (   
                        
                        this.state.doc === true ?
                        (<div><FileContract className=" section col s12" /><p>Document déja uploader</p></div>)
                        :
                        (<div><FileContract className=" section col s12" /><p>Pas de document uploader</p></div>)
                    )
                    :
                    (
                        this.state.doc === true ?
                        (<a href={this.state.docu} download> FICHIER PDF</a>)
                        :
                        (<p>Pas de document uploader</p>)
                    )
                }
            </div>           
        );
    }
};



const mapStateToProps = (state,props)=>{

    let company = [];
    let contract = [] ;
    if( sessionStorage.getItem('auth')==='student'){
        company = state.enterprises;
        contract = state.contracts;
    }else{
       company = state.enterprises //.filter((r)=> r.StudentId.toString() === props.match.params.id);
       contract = state.contracts //.filter((contract)=> contract.StudentId === props.match.params.id);
    }
     

    return{
        enterprises: company,
        contracts: contract
          }
    };



export default connect(mapStateToProps)(ListEntreprise);
