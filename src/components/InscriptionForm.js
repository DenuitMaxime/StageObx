import React from 'react';



export default class InscriptionForm extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            mail:'',
            name:'',
            firstName:'',
            phone:'',
            pwd1:'',
            pwd2:'',
            error:''
        }
    }
    onCommonChange=(e)=>{
        const target= e.target;
        const value = target.value;
        const name = target.name;
        this.setState(()=>({[name]:value}));
    }
    onSubmit=(e)=>{
        e.preventDefault();
        if(!this.state.mail || !this.state.name || !this.state.firstName || !this.state.phone || !this.state.pwd1 || !this.state.pwd2){
            this.setState(()=>({error:'Veuillez remplir tout les champs'}));
        }else if(this.state.pwd1 !== this.state.pwd2){
            this.setState(()=>({error:'les deux mots de passes doivent être identique'}));
        }else if(!this.state.mail.match(/^.+@student\.vinci\.be$/)){
            console.log(this.state.mail.match(/^.+@student\.vinci\.be$/))
            this.setState(()=>({error:"l'adresse mail doit etre de type exemple@student.vinci.be"}));
        }else if(!this.state.pwd1.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&.])[A-Za-z\d$@$!%*?&.]{6,}/) ){
            this.setState(()=>({error:'le mot de passe doit avoir au moins 6 caractères, 1 majusucle, 1 chiffres et 1 caractères spécial'}));
        }else{
            this.setState(()=>({error:''}));
            this.props.onSubmit({
                mail:this.state.mail,
                name:this.state.name,
                firstName:this.state.firstName,
                phone:this.state.phone,
                pwd1:this.state.pwd1,
                pwd2:this.state.pwd2
            })
        }
    }
    render(){
        return(
            <div className="row">
                {this.state.error && <p className="flex-text bold red-text">{this.state.error}</p>}
                <form className="col s12" onSubmit={this.onSubmit}>
                    <div className="row section">
                    
                        <input
                            className="validate"
                            placeholder="Mail*"
                            type='email'
                            name='mail'
                            value={this.state.mail}
                            onChange={this.onCommonChange}/>
                            
                    
                        <input
                            className="validate" 
                            type='text'
                            placeholder='Nom*'
                            name='name'
                            value={this.state.name}
                            onChange={this.onCommonChange}/>
                        <input 
                            className="validate"
                            type='text'
                            placeholder='Prénom*'
                            name='firstName'
                            value={this.state.firstName}
                            onChange={this.onCommonChange}/>
                        <input 
                            className="validate"    
                            type='text'
                            placeholder='Téléphone*'
                            name='phone'
                            value={this.state.phone}
                            onChange={this.onCommonChange}/>      
                        <input
                            className="validate"    
                            type='password'
                            placeholder='Mot de passe*'
                            name='pwd1'
                            value={this.state.pwd1}
                            onChange={this.onCommonChange}/>
                        <input
                            className="validate"
                            placeholder='Vérification du mot de passe*'
                            type='password'
                            name='pwd2'
                            value={this.state.pwd2}
                            onChange={this.onCommonChange}/> 
                        
                        </div>  
                        <button className="btn blue-grey white-text">S'inscrire</button>
                </form>
            </div>
        )
    }
}