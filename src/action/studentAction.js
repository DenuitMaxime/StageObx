export const editEnterprise=(update,id)=>({
    type:'EDIT_ENTERPRISE',
    update,
    id
});

export const addEnterprise=(enterprise)=>({
    type:'ADD_ENTERPRISE',
    enterprise,
});

export const changeContract=(update,id)=>({
    type:'CHANGE_CONTRACT',
    update,
    id 
});

export const addAllContract=(update)=>({
    type:'ADD_ALL_CONTRACT',
    update
});

export const deleteContract=(id)=>({
    type:'DELETE_CONTRACT',
    id
});

export const addAll=(update)=>({
    type:'ADD_ALL_STU',
    update
});

export const addAllComp=(update)=>({
    type:'ADD_ALL_COMP',
    update
});
export const resetStore=()=>({
    type:'RESET',
})